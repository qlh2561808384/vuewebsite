export default {
    USER_AUTH_KEY : "Authorization",
    //高级管理员的标识符，并不是改了这个就成为高级管理员了，仅仅是一个标识符。
    ADMIN: "ADMIN",
    authObj: null,
    /**
     * 可以直接调用此方法获得当前登录人
     * @returns {null}
     */
    getUserName() {
        return this._getVal("username");
    },
    _getVal(val) {
        if (this.authObj == null) {
            //console.log("加载sessionStorage中的userAuth信息......");
            this._getAuth();
        }
        return this.authObj ? this.authObj[val] : null;
    },
    _getAuth() {
        let authString = window.sessionStorage.getItem(this.USER_AUTH_KEY);
        if (authString) {
            this.authObj = JSON.parse(authString);
        }
    },
    //以字符串返回，且转换了ADMIN为超级管理员。
    getAuthValueString() {
        let arr =  this.getAuthValueArr()
        let str = arr.map(item=> (item == this.ADMIN)? "超级管理员" : item );
        return str.toString();
    },

    //以数组返回,且过滤了"ROLE_"
    getAuthValueArr() {
        let arr =  this.getAuth();
        let str = [];
        arr.forEach(item =>{
            str.push(item.authority.replace("ROLE_",""));
        });
        return str;
    },
    /**
     * 可以直接调用此方法获得角色名称
     * @returns {null}
     */
    getAuth(){
        return this._getVal("auth");
    },
    /**
     * 保存用户基本权限信息
     * @param auth
     */
    setAuth(auth) {
        if (auth) {
            let authString = JSON.stringify(auth);
            window.localStorage.setItem(this.USER_AUTH_KEY, authString);
        }
    }
}
