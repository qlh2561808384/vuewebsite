// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
//后缀名.js.vue的话 import的话直接省略后缀名
import Vue from "vue"; //导入vue
import App from "./App"; //导入App组件
import router from "./router"; //导入route组件 如果导入router文件夹下面的其他js的话就必须写出来。如果是index.js的话不用写index.js
import axios from "axios"; //导入axios组件
// import VueAxios from "vue-axios"; //导入vue-axios组件
import ElementUI from "element-ui"; //加载element-ui前端ui框架
import "element-ui/lib/theme-chalk/index.css"; //加载element-ui前端ui框架样式
import VueAwesomeSwiper from 'vue-awesome-swiper'//导入轮播图vue-awesome-swiper
import 'swiper/css/swiper.css'//需要导入swiper 导入样式
import './http/axios'// axios 拦截器

Vue.use(VueAwesomeSwiper);
Vue.use(ElementUI);
// Vue.use(VueAxios, axios);
Vue.config.productionTip = false;

/* eslint-disable no-new */
//初始化挂载点的Vue
new Vue({
  el: "#app",
  router, //路由
  components: {
    App
  },
  template: "<App/>"
});
