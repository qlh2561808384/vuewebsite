/*
    用户 请求代理配置
*/
// 后端请求地址 注意[他会根据你环境的不同从而获取的 env 文件不同]
const target = process.env.APP_API_URL;

module.exports = {
    devServer: {
        // 所有的接口请求代理
        proxy: {
            '/api': {
                target: target,
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    '^api': ''
                }
            }
        }
    }
}