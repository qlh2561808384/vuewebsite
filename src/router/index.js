import Vue from "vue"; //导入vue组件
import Router from "vue-router"; //导入路由route组件
import Login from "../components/login/Login"; //导入自己写的模板组件
import Register from "../components/register/Register";
import Home from "../components/home/Home";
import imageList from "../components/imageList";
import AliPay from "../components/AliPay";
import CarouselSwiper from "../components/util/CarouselSwiper";
import About from "../components/adout/About"

Vue.use(Router);

export default new Router({
  routes: [{
    path: "/",
    name: "Home",
    redirect: "/main",
    component: Home
  },
    {
      path: "/main",
      name: "Home",
      component: Home,
      children: [{
        path: "/main/about",
        name: "About",
        component: About
      }, {
        path: "/main/CarouselSwiper",
        name: "CarouselSwiper",
        component: CarouselSwiper
      }, {
        path: "/main/imageList",
        name: "imageList",
        component: imageList
      },
        {
          path: "/main/AliPay",
          name: "AliPay",
          component: AliPay
        },]
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/register",
      name: "Register",
      component: Register
    }
  ],
  mode: 'history' //去掉路径中的#
});
